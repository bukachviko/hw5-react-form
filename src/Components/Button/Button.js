import React from "react";

const Button = ({btnClass, backgroundColor, text, myClick}) => {
    return (
        <button
            className={btnClass}
            onClick= {myClick}
            style={{backgroundColor}}
        >
            {text}
        </button>
    )
}

export default Button;