import React from 'react';
import './FormGetInfo.css';
import {Field, Form, Formik} from "formik";
import {formSchema} from "./FormSchema";
import {clearCartFromLocalStorage, clearProductToCard} from "../Slices/productsSlice"
import {useDispatch, useSelector} from "react-redux";
import {PatternFormat} from 'react-number-format';

const initialValues = {
    userFirstName: "",
    userLastName: "",
    userAge: "",
    userDeliveryAddress: "",
    userPhone: ""
}

function FormGetInfo(props) {
    const dispatch = useDispatch();
    const cartItems = useSelector(state => state.products.cart);

    const handleSubmit = (values, {resetForm}) => {
        handleCheckout(values);
        resetForm();

    }

    const handleCheckout = (formData) => {
        dispatch(clearCartFromLocalStorage());
        console.log("Інформація про замовлення:", formData);
        console.log("Придбанні товари: ", cartItems);
        dispatch(clearProductToCard());

        alert("Ваше замовлення прийняте!")
    }

    return (
        <>
            <Formik
                initialValues={initialValues}
                validationSchema={formSchema}
                onSubmit={handleSubmit}
            >
                {({errors}) => (
                    <Form>
                        <h1>Швидке замовлення</h1>
                        <label htmlFor="userFirstName">Ваше ім`я</label>
                        <br/>
                        <Field
                            type="text" name="userFirstName"/>
                        <br/>
                        {errors.userFirstName && <p>{errors.userFirstName}</p>}
                        <br/>
                        <label htmlFor="userLastName">Ваше прізвище</label>
                        <br/>
                        <Field type="text" name="userLastName"/>
                        <br/>
                        {errors.userLastName && <p>{errors.userLastName}</p>}
                        <br/>
                        <label htmlFor="userAge">Ваш вік</label>
                        <br/>
                        <Field type="number" name="userAge"/>
                        <br/>
                        {errors.userAge && <p>{errors.userAge}</p>}
                        <br/>
                        <label htmlFor="userDeliveryAddress">Адреса доставки</label>
                        <br/>
                        <Field type="text" name="userDeliveryAddress"/>
                        <br/>
                        {errors.userDeliveryAddress && <p>{errors.userDeliveryAddress}</p>}
                        <br/>
                        <label htmlFor="userPhone">Мобільний телефон</label>
                        <br/>
                        <Field type="number" name="userPhone">
                            {({field,form: { touched, errors },meta}) => (
                                <PatternFormat name="userPhone" format="+38 (0##) #### ###" allowEmptyFormatting mask="_" {...field} />
                            )}
                        </Field>
                        <br/>
                        {errors.userPhone && <p>{errors.userPhone}</p>}
                        <br/>
                        <button type="submit">Checkout</button>
                    </Form>
                )}
            </Formik>
        </>
    );
}

export default FormGetInfo;