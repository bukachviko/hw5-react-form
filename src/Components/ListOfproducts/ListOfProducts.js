import ProductCard from "../ProductCard/ProductCard";
import {useDispatch, useSelector} from 'react-redux';
import React, {useEffect} from "react";
import {addToCart, fetchCardData} from '../Slices/productsSlice';
import Modal from "../Modal/Modal";
import {closeModal} from "../Slices/modalSlice";

const ListOfProducts = () => {
    const dispatch = useDispatch();
    const isShowModal = useSelector(state => state.modal.isShowModal);
    const data = useSelector(state => state.products.data);
    const status = useSelector((state) => state.products.status);
    const error = useSelector((state) => state.products.error);
    const cart = useSelector((state) => state.products.cart)
    const currentProductToCart = useSelector((state) => state.products.currentProductIdToCart)

    const productAddToCard = () => {
        if (!(cart.filter((item) => item.id === currentProductToCart)).length) {
            dispatch(addToCart(data.filter((item) => item.id === currentProductToCart)[0]))
        }

        dispatch((closeModal()))
    }

    useEffect(() => {
        dispatch(fetchCardData());

    }, [dispatch]);

    return (
        <>
            <ul className="card__container">
                {status === 'loading' && <div>Loading...</div>}
                {status === 'failed' && <div>Error: {error}</div>}
                {status === 'succeeded' && data.length > 0 ? (
                    data.map((item, index) => {

                           let existEl = cart.find((element) => {
                                return element.id === item.id
                            });

                        let classes = 'card';

                           if (typeof existEl === "object") {
                               classes += " added-in-cart";
                           }

                           return <li className={classes} key={index}>
                                <ProductCard
                                    image={item.image}
                                    name={item.name}
                                    color={item.color}
                                    cost={item.cost}
                                    article={item.article}
                                    productId={item.id}
                                />
                            </li>
                        }
                    )
                ) : (
                    <div>No data available</div>
                )}
            </ul>

            {isShowModal && <Modal
                classNameBtn={"add__product-bnt"}
                backgroundColor={{backgroundColor: '#833ab4'}}
                header={"Додати товар в кошик?"}
                text={"Можливе замовлення від 1 шт"}
                boolean={true}
                nameBtn1={'Добре!'}
                nameBtn2={'Ні!'}
                functionSuccess={productAddToCard}
            />}
        </>
    );
}

export default ListOfProducts;
