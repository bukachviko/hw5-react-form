import {createSlice} from "@reduxjs/toolkit";

const modalSlice = createSlice({
    name: "modal",
    initialState: {
        isShowModal: false
    },
    reducers: {
        openModal:(state) => {
            state.isShowModal = true
        },
        closeModal:(state) => {
            state.isShowModal = false
        },
    }
})

export default modalSlice.reducer;
export const {openModal, closeModal} = modalSlice.actions